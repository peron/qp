/**
    @file qp_dbg.h
    @brief Debugging stuff.

    Copyright (c) 2017 Phil Peron

    qp is covered under the MIT license in its entirety, not including 3rd party components.
    Refer to LICENSE.txt for more information.
*/

#ifndef QP_DBG
#define QP_DBG

#include <stdio.h>
#include "qp.h"

namespace qp {
namespace dbg {

/**
    @brief Prints the bits of a 64-bit integer as a string of 1s and 0s. Useful for debugging bit-twiddly things.

    @param v A 64-bit integer.

    @return void
*/
void print_bits(u64 v)
{
    char buffer[64];

    for (int i = 0; i < 64; i++) {
        if (v & 1) buffer[i] = 1;
        else buffer[i] = 0;
        v >>= 1;
    }

    for (int i = 63; i >= 0; i--) {
        printf("%d", buffer[i]);
    }
}

} // namespace dbg
} // namespace qp

#endif // QP_DBG