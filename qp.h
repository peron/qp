/**
    @file qp.h
    @brief General purpose stuff.

    Copyright (c) 2017 Phil Peron

    qp is covered under the MIT license in its entirety, not including 3rd party components.
    Refer to LICENSE.txt for more information. 
*/

#ifndef QP_H
# define QP_H


namespace qp {

// The following definitions exist because doxygen isn't respecting the
// conditional definitions. Or maybe I'm not respecting doxygen. Hmm...
/// An 8-bit integer.
# define i8
/// An unsigned 8-bit integer.
# define u8
/// A 16-bit integer.
# define i16
/// An unsigned 16-bit integer.
# define u16
/// A 32-bit integer.
# define i32
/// An unsigned 32-bit integer.
# define u32
/// A 64-bit integer.
# define i64
/// An unsigned 64-bit integer.
# define u64


#ifdef _WIN32
# define i8     __int8
# define u8     unsigned __int8
# define i16    __int16
# define u16    unsigned __int16
# define i32    __int32
# define u32    unsigned __int32
# define i64    __int64
# define u64    unsigned __int64

#elif __linux__ && __clang__

# define i8     i8
# define u8     unsigned i8
# define i16    i16
# define u16    unsigned i16
# define i32    i32
# define u32    unsigned i32
# define i64    i64
# define u64    unsigned i64

#elif __linux__ && __GNUC__

# define i8     int8_t
# define u8     uint8_t
# define i16    int16_t
# define u16    uint16_t
# define i32    int32_t
# define u32    uint32_t
# define i64    int64_t
# define u64    uint64_t

#endif

/// A 32-bit, floating-point number.
#define f32	float
/// A 64-bit, floating-point number.
#define f64	double


} // namespace qp

#endif