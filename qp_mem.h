/**
    @file qp_mem.h
    @brief Memory management stuff.

    Copyright (c) 2017 Phil Peron

    qp is covered under the MIT license in its entirety, not including 3rd party components.
    Refer to LICENSE.txt for more information.
*/

#ifndef QP_MEM_H
# define QP_MEM_H


#include <stdlib.h>
#include "qp.h"
#include "qp_math.h"


#ifndef NDEBUG
#include <stdio.h>
#endif // NDEBUG



namespace qp {
namespace mem {


/// @cond
#define MIN_BANK_SIZE           64 // Must be 2^6 for minimum low order block size of 2^0.
#define MAX_BANK_ORDER          6
#define BANK_REGISTRY_WIDTH     64
#define BANK_ORDER_WIDTH        4
#define BANK_ORDER_MASK         0xF000000000000000
/// @endcond


/**
    @struct bank_t
    @brief Banks are composed of a contiguous block of memory and two bookkeeping constructs. Their primary use is inside of caches.
*/
struct bank_t {
    u64 size; // Total size of contiguous block.
    u64 registry; // Bit field that tracks free/used blocks. Each bit represents a low order block size.
    u64 order_entries[4]; // Tracks order size of the index of an allocation request. Each entries is 4 bits.
    void *base_addr; // Stores the value of the pointer to memory received by the internal malloc request.
    u32 low_order_block_size; // Size of the lowest order available. (4,294,967,295 order 0 bytes would support 274,877,906,944 order 6 bytes [or 256 GiB] of memory.)
    u8 high_order;
    u8 low_order;
    char padding[2];
};


/// @cond
struct bank_node_t {
    bank_t *bank;
    bank_node_t *next;
    bank_node_t *prev;
    u64 mask;
};
/// @endcond


/**
    @struct cache_t
    @brief A cache is a collection of banks. Caches are used when a user wants more than 6 orders of size selections from the initial contiguous block.
*/
struct cache_t {
    void *base_addr = NULL;
    u64 size = 0;
    u64 used = 0;
    u64 free = 0;
    bank_node_t *head_bank_node = NULL;
    bank_node_t *tail_bank_node = NULL;
};


/// @cond
struct bank_registry_mask_t {
    u64 field;
    u8 width;
    u8 limit;
    char padding[6];
};
/// @endcond


/**
    @brief Create and return an initialized bank structure.

    @param ptr A pointer to a block of memory.
    @param size The size of the block of memory in bytes. This value must match the size of the allocation represented by ptr and also be a power of two.

    @return A pointer to an initialized bank_t structure.
*/
bank_t * bank_create(void *ptr, const size_t size);


/**
    @brief Destroys a bank and frees its memory.
    
    @param bank A pointer to a bank to destroy.

    @return void
*/
void bank_destroy(bank_t *bank);


/**
    @brief Request a block of memory from a bank.

    @param bank A pointer to a bank.
    @param size The amount of memory requested. This value is rounded up to the nearest power of two.

    @return A void * pointer to a block of memory
*/
void * bank_alloc(bank_t *bank, const size_t size);


/**
    @brief Frees a block of memory previously allocated by a bank.

    @param bank A pointer to a bank.
    @param ptr A pointer to the block of memory to free.

    @return void 
*/
u64 bank_free(bank_t *bank, void *ptr);


/**
    @brief Create a new cache and initialize the cache's banks.

    @param size The size of the block of memory in bytes. This is rounded up to the nearest power of two value.

    @return A pointer to an initialized cache_t structure.
*/
cache_t * cache_create(const size_t size);


/**
    @brief Destroy a cache and free its memory.

    @param cache A pointer to the cache to destroy.

    @return void
*/
void cache_destroy(cache_t *cache);


/**
    @brief Request a block of memory from a cache.

    @param cache A pointer to the cache to request memory from.
    @param size The amount of memory requested. This value is rounded up to the nearest power of two.
    
    @return A void * pointer to a block of memory
*/
void * cache_alloc(cache_t *cache, const size_t size);


/**
    @brief Frees a block of memory previously allocated by a cache.

    @param cache A pointer to a cache.
    @param ptr A pointer to the block of memory to free.

    @return void
*/
void cache_free(cache_t *cache, void *ptr);


/// @cond
static const bank_registry_mask_t bank_registry_masks[7] = {
    { 0x8000000000000000, 1, 64 },	// ORDER 0
    { 0xC000000000000000, 2, 32 },	// ORDER 1
    { 0xF000000000000000, 4, 16 },	// ORDER 2
    { 0xFF00000000000000, 8, 8 },	// ORDER 3
    { 0xFFFF000000000000, 16, 4 },	// ORDER 4
    { 0xFFFFFFFF00000000, 32, 2 },	// ORDER 5
    { 0xFFFFFFFFFFFFFFFF, 64, 1 }	// ORDER 6
};
/// @endcond


bank_t * bank_create(void *ptr, const size_t size)
{
    if (!ptr || size < MIN_BANK_SIZE) return NULL;

    bank_t *bank = (bank_t *)malloc(sizeof(bank_t));
    if (bank) {
        bank->size = size;
        bank->base_addr = ptr;
        bank->high_order = math::log2_64(bank->size);
        bank->low_order = (u8)(bank->high_order - MAX_BANK_ORDER);
        bank->low_order_block_size = (1ULL << bank->low_order);
        bank->registry = 0x0;
        bank->order_entries[0] = 0xFFFFFFFFFFFFFFFF;
        bank->order_entries[1] = 0xFFFFFFFFFFFFFFFF;
        bank->order_entries[2] = 0xFFFFFFFFFFFFFFFF;
        bank->order_entries[3] = 0xFFFFFFFFFFFFFFFF;

        return bank;
    }

    return NULL;
}


void bank_destroy(bank_t *bank)
{
    free(bank);
    bank = NULL;
}


void * bank_alloc(bank_t *bank, const size_t size)
{
    if (!bank || size > bank->size) return NULL;

    i32 order = math::log2_64(math::ceil_po2(size)) - bank->low_order;
    order = (order < 0) ? 0 : order;
    bank_registry_mask_t mask = bank_registry_masks[order];

    for (u32 i = 0; i < mask.limit; i++) {
        u32 index = mask.width * i;
        u64 mask_field = mask.field >> index;

        if ((mask_field & bank->registry) == 0) {
            bank->registry |= mask_field;
            u32 entries_index = index >> 4;
            u32 entries_bitpos = (index & 0xF) * BANK_ORDER_WIDTH;
            u64 cur_order_entry = bank->order_entries[entries_index];
            u64 new_order_entry = ((u64)order << ((BANK_REGISTRY_WIDTH - BANK_ORDER_WIDTH) - entries_bitpos));
            u64 order_mask = ~(BANK_ORDER_MASK >> entries_bitpos);
            new_order_entry |= order_mask;
            cur_order_entry &= new_order_entry;
            bank->order_entries[entries_index] = cur_order_entry;

            uintptr_t addr = (uintptr_t)bank->base_addr + (bank->low_order_block_size * index);
            return (void *)addr;
        }
    }

    return NULL;
}


u64 bank_free(bank_t *bank, void *ptr)
{
    if (!bank || (ptr == 0)) return NULL;

    u32 index = (u32)(((uintptr_t)ptr - (uintptr_t)bank->base_addr) / bank->low_order_block_size);
    u32 entries_index = index >> 4;
    u32 entries_bitpos = (index & 0xF) * BANK_ORDER_WIDTH;
    u64 cur_order_entry = bank->order_entries[entries_index];
    u64 order_mask = BANK_ORDER_MASK >> entries_bitpos;
    u32 order = (u32)((cur_order_entry & order_mask) >> ((BANK_REGISTRY_WIDTH - BANK_ORDER_WIDTH) - entries_bitpos));
    cur_order_entry |= order_mask;
    bank->order_entries[entries_index] = cur_order_entry;
    bank_registry_mask_t mask = bank_registry_masks[order];
    bank->registry &= ~(mask.field >> index);
    ptr = NULL;

	return 1ULL << (order + bank->low_order);
}


cache_t * cache_create(const size_t size)
{
    cache_t *cache = (cache_t *)malloc(sizeof(cache_t));
    if (!cache) return NULL;

    cache->size = (size > MIN_BANK_SIZE) ? math::ceil_po2(size) : MIN_BANK_SIZE;
    cache->base_addr = malloc(cache->size);
    if (cache->base_addr == NULL) {
        free(cache);
        return NULL;
    }
    cache->free = cache->size;
    cache->used = 0;

    cache->head_bank_node = (bank_node_t *)malloc(sizeof(bank_node_t));
    if (cache->head_bank_node == NULL) {
        free(cache->base_addr);
        free(cache);
        return NULL;
    }
    bank_node_t *cur_node = cache->head_bank_node;
    bank_node_t *prev_node = NULL;

    u8 order = math::log2_64(cache->size);
    const i32 num_banks = order / MAX_BANK_ORDER;

    for (i32 i = 0; i < num_banks; i++) {
        size_t s = 1ULL << order;
        cur_node->bank = bank_create(cache->base_addr, s);
        cur_node->next = NULL;
        cur_node->prev = prev_node;
        cur_node->mask = cur_node->bank->size - cur_node->bank->low_order_block_size;
        cache->tail_bank_node = cur_node;
        s >>= MAX_BANK_ORDER;
        if (s >= MIN_BANK_SIZE) {
            cache->base_addr = bank_alloc(cur_node->bank, s >> MAX_BANK_ORDER);
            cur_node->next = (bank_node_t *)malloc(sizeof(bank_node_t));
            if (cur_node->next == NULL) {
                // WARNING: Ugh. If it fails here, the world is ending.
                // You could roll the memory back and clean up but... well, the world is ending.
                return NULL;
            }
            prev_node = cur_node;
            cur_node = cur_node->next;
        }
        order -= MAX_BANK_ORDER;
    }

    return cache;
}


void * cache_alloc(cache_t *cache, const size_t size)
{
    u64 req_size = math::ceil_po2(size);
    if (req_size >= cache->size) return NULL;

    bank_node_t *cur_node = cache->head_bank_node;
    while (cur_node->next) {
        if (cur_node->mask & req_size) break;
        cur_node = cur_node->next;
    }

	void * ret_val = bank_alloc(cur_node->bank, req_size);

	if (ret_val) {
		cache->free -= req_size;
		cache->used += req_size;
	}

    return ret_val;
}


void cache_free(cache_t *cache, void *ptr)
{
    if (!cache || (ptr == 0)) return;

    u64 addr_offset = (uintptr_t)ptr - (uintptr_t)cache->base_addr;

    bank_node_t *cur_node = cache->head_bank_node;
    while (cur_node->next) {
        if (cur_node->mask & addr_offset) break;
        cur_node = cur_node->next;
    }

	u64 freed = bank_free(cur_node->bank, ptr);

	cache->used -= freed;
	cache->free += freed;
}


void cache_destroy(cache_t *cache)
{
    bank_node_t *pn = cache->tail_bank_node;
    bank_node_t *tmp_pn = NULL;

    do {
        bank_free(pn->prev->bank, pn->bank->base_addr);
        bank_destroy(pn->bank);
        tmp_pn = pn->prev;
        free(pn);
        pn = tmp_pn;
    } while (pn->prev);

    bank_destroy(cache->head_bank_node->bank);
    free(cache->head_bank_node);
    cache->head_bank_node = NULL;

    free(cache->base_addr);
    cache->base_addr = NULL;

    free(cache);
    cache = NULL;
}

} // namespace mem
} // namespace qp

#endif // QP_MEM