/**
    main.cpp
    Primarily used to test qp.

    Copyright (c) 2017 Phil Peron

    qp is covered under the MIT license in its entirety, not including 3rd party components.
    Refer to LICENSE.txt for more information.
*/

#include <stdlib.h>
#include <assert.h>
#include "qp.h"
#include "qp_mem.h"

using namespace qp;

void run_bank_test()
{
    mem::bank_t *b = NULL;
    size_t size = 1000;
    size = (size > MIN_BANK_SIZE) ? math::ceil_po2(1000) : MIN_BANK_SIZE;
    void *ptr = malloc(size);
    if (ptr) {
        b = mem::bank_create(ptr, size);
        if (b) {
            void *my_addr = bank_alloc(b, 100);
            void *my_addr2 = bank_alloc(b, 500);
            void *my_addr3 = bank_alloc(b, 250);
            void *my_addr4 = bank_alloc(b, 16);
            void *my_addr5 = bank_alloc(b, 64);

            assert(b->registry == 0xFF8FFFFFFFFFFFFF);
            assert(b->order_entries[0] == 0x3FFFFFFF0FFF2FFF);
            assert(b->order_entries[1] == 0x4FFFFFFFFFFFFFFF);
            assert(b->order_entries[2] == 0x5FFFFFFFFFFFFFFF);
            assert(b->order_entries[3] == 0xFFFFFFFFFFFFFFFF);

            bank_free(b, my_addr);
            bank_free(b, my_addr2);
            bank_free(b, my_addr3);
            bank_free(b, my_addr4);
            bank_free(b, my_addr5);

            assert(b->registry == 0x0);
            assert(b->order_entries[0] == 0xFFFFFFFFFFFFFFFF);
            assert(b->order_entries[1] == 0xFFFFFFFFFFFFFFFF);
            assert(b->order_entries[2] == 0xFFFFFFFFFFFFFFFF);
            assert(b->order_entries[3] == 0xFFFFFFFFFFFFFFFF);

            bank_destroy(b);
			free(ptr);
        }
        else abort();
    }
    else abort();
}


// TODO: Write proper test for caches.
void run_cache_test()
{
    mem::cache_t *cache = mem::cache_create(1000000000);
	u64 orig_free = cache->free;
	u64 orig_used = cache->used;
    void *p1 = cache_alloc(cache, 30000);
    void *p2 = cache_alloc(cache, 4572342);
    void *p3 = cache_alloc(cache, 10);
    cache_free(cache, p1);
    cache_free(cache, p2);
    cache_free(cache, p3);
	assert(cache->free == orig_free);
	assert(cache->used == orig_used);
    cache_destroy(cache);
}

int main()
{
    run_bank_test();
    run_cache_test();

    return EXIT_SUCCESS;
}