/**
    @file qp_math.h
    @brief Math stuff.

    Copyright (c) 2017 Phil Peron

    qp is covered under the MIT license in its entirety, not including 3rd party components.
    Refer to LICENSE.txt for more information.
*/

#ifndef QP_MATH
#define QP_MATH


#include "qp.h"


namespace qp {
namespace math {


inline u8 log2_64(u64 v);
inline u64 ceil_po2(u64 v);

/// @cond
// Find log2 of 64-bit integer courtesy of Desmond Hume:
// http://stackoverflow.com/questions/11376288/fast-computing-of-log2-for-64-bit-integers
const u8 tab64[64] = {
    63,  0, 58,  1, 59, 47, 53,  2, 60, 39, 48, 27, 54, 33, 42,  3,
    61, 51, 37, 40, 49, 18, 28, 20, 55, 30, 34, 11, 43, 14, 22,  4,
    62, 57, 46, 52, 38, 26, 32, 41, 50, 36, 17, 19, 29, 10, 13, 21,
    56, 45, 25, 31, 35, 16,  9, 12, 44, 24, 15,  8, 23,  7,  6,  5 };
/// @endcond
/**
    @brief Returns the base-2 logarithm of the specified 64-bit value.

    @param v 64-bit integer.

    @return The base-2 logarithm of the 64-bit integer argument.
*/
inline u8 log2_64(u64 v)
{
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v |= v >> 32;
    return tab64[((v - (v >> 1)) * 0x07EDD5E59A4E28C2) >> 58];
}


// Round up to nearest power of 2 courtesy of Sean Anderson:
// https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
/**
    @brief Calculates the ceiling of a 64-bit integer to the nearest power of two.

    @param v A 64-bit integer.

    @return A rounded, power of two value of the 64-bit integer argument.
*/
inline u64 ceil_po2(u64 v)
{
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;

    return v;
}

} // namespace math
} // namespace qp

#endif // QP_MATH